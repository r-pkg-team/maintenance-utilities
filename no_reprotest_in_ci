#!/bin/sh

CIFILE=debian/salsa-ci.yml

SKIPCI=""
if [ "$1" = "-s" -o "$1" = "--skipci" ] ; then
  SKIPCI='[skip ci] '
fi

if [ ! -d debian ] ; then
  echo "No debian/ dir found"
  exit 1
fi

if [ ! -e "$CIFILE" ] ; then
  echo "Repository does not contain $CIFILE"
  exit 1
fi

if grep -q '^#  SALSA_CI_DISABLE_REPROTEST: 1' "$CIFILE" ; then
  sed -i \
      -e '/^# R creates .rdb files and .rds with some randomness./d' \
      -e '/^# https:\/\/tests.reproducible-builds.org\/debian\/issues\/unstable\/randomness_in_r_rdb_rds_databases_issue.html/d' \
      -e '/^# Uncomment the following two lines if this package is affected./d' \
      -e '/^#variables:/d' \
      -e '/^#  SALSA_CI_DISABLE_REPROTEST: 1/d' \
      "$CIFILE"
fi

if grep -q SALSA_CI_DISABLE_REPROTEST "$CIFILE" ; then
  # salsa-ci just contains SALSA_CI_DISABLE_REPROTEST string
  echo "String SALSA_CI_DISABLE_REPROTEST is found in $CIFILE.  Please check manually"
  exit 0
fi

cat >>"$CIFILE" <<EOT
# R creates .rdb files and .rds with some randomness.
# https://tests.reproducible-builds.org/debian/issues/unstable/randomness_in_r_rdb_rds_databases_issue.html
# Thus reprotest is disabled here
variables:
  SALSA_CI_DISABLE_REPROTEST: 1
EOT

DCHOPT=""
if head -n1 debian/changelog | grep -qw unstable ; then
  DCHOPT="-i"
fi
dch $DCHOPT "Disable reprotest"
git commit -a -m"${SKIPCI}Disable reprotest"

exit 0
