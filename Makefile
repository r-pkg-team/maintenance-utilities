#!/usr/bin/make -f
#
# This Makefile creates a PNG graph of the relation of BioConductor files.
# It is created from BioC_dependency-tree.ditaa and you need to install the ditaa package

NAME:=BioC_dependency-tree

all: png

png: $(NAME).png

%.png: %.ditaa
	rm -f $@
	ditaa $^

clean:
	rm -f $(NAME).png
