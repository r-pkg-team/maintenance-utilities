#!/bin/bash
set -e
set -u
set -o pipefail
set -x

BIOC_VERSION=3.20
LEVEL=$1

# requirements
# 1. routine-update, git, devscripts, git-buildpackage
# 2. team membership in Debian R Team
# 3. Debian Developer uploading privilages

# assumptions:
# 0. The root folder for packaging work is ~/debian/
# 1. All r-bioc-* packages are already checked out at ~/debian/
# 2. sbuild is used for package building

# Why is this difficult?
# It isn't just that we need to update the package, routine-update takes care of most of that.
# It is that we have to record our changes in the team maintained VCS
# which might already have a 'debian/experimental' branch.
# So this script tries to be careful; to avoid overwriting existing or in-progress work.


checkout_experimental() {
	git checkout debian/experimental && { git pull origin debian/experimental || { echo "E: Handle ${pkg} manually" ; exit 1 ; } ; } \
		|| git checkout "$(git rev-parse --abbrev-ref origin/HEAD | sed 's=origin/==')" \
			&& git checkout -b debian/experimental && return 0
	
	# is this newer than what is released? If so, process manually
	if dpkg --compare-versions "$(dpkg-parsechangelog -SVersion)" gt "$(rmadison "${pkg}" -s unstable | awk '{ print $3}')" 
	then
		read -rp "Package ${pkg} is already newer than the version in stable. Process manually and then press return to continue." res
		case $res in
			''|[YyJj]* )
				echo "I: Continuing after ${pkg}"
				;;
			*)
				echo "W: Exiting, don't forget to update the list of processed packages"
				exit 1
				;;
		esac
	fi
	git rev-list "@{u}..@" --count | grep 0
	git reset --hard "$(git rev-parse --abbrev-ref origin/HEAD)"
	git push --force origin
}

for pkg in $(cat bioc-${BIOC_VERSION}-level-${LEVEL}-packages.txt)
do
	echo "I: Processing package ${pkg}"
	cd "${HOME}/debian/${pkg}"
	git fetch --all
	git checkout "$(git rev-parse --abbrev-ref origin/HEAD | sed 's=origin/==')"
	gbp pull
	checkout_experimental
	export GBP_BUILDPACKAGE_EXTRA=--git-builder="sbuild --extra-repository='deb https://deb.debian.org/debian experimental main' --build-dep-resolver=aspcud"
	routine-update --experimental --branch debian/experimental || { unset GBP_BUILDPACKAGE_EXTRA ; git checkout -- . ; routine-update --experimental --branch debian/experimental -u ; }
	git clean -fdx && dpkg-buildpackage -S -d
	debrelease  -S --dput && gbp push --debian-branch=debian/experimental
done
