#!/usr/bin/python3

from sys import argv, exit, stderr
import re

depends_keys = ('LinkingTo', 'Imports', 'Depends')
# r namespaces included in r-base-core which we shouldn't try and ignore in dependencies

deps_ignore = ('R', 'base', 'compiler', 'datasets', 'graphics', 'grDevices', 'grid', 'methods',
               'parallel', 'splines', 'stats', 'stats4', 'tcltk', 'tools', 'translations', 'utils')

def read_description_file(file_path):
    description = {}
    current_key = None
    current_value = []

    with open(file_path, 'r') as file:
        for line in file:
            line = line.rstrip()

            if not line:
                continue

            if line.startswith(' ') or line.startswith('\t'):
                current_value.append(line.lstrip())
            else:
                if current_key is not None:
                    description[current_key] = ' '.join(current_value)

                parts = line.split(':')
                current_key = parts[0].strip()
                try:
                    current_value = [parts[1].lstrip()]
                except IndexError as err:
                    print(parts)
                    stderr.write("Problem parsing %s\n" % file_path)
                    exit(1)

        if current_key is not None:
            description[current_key] = ' '.join(current_value)

    return description

if len(argv) < 2 :
    stderr.write("Missing DESCRIPTION file")
    exit(1)

file_path = argv[1]
description_data = read_description_file(file_path)

for key, value in description_data.items():
  if key in depends_keys:
    for dep in value.split(','):
      dep = dep.strip()
      dep = re.sub(' *\(.*\)', '', dep)
      if dep not in deps_ignore:
        print(dep)
