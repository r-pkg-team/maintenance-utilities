#!/bin/bash
set -e
set -u
set -o pipefail
set -x

BIOC_VERSION=3.20
LEVEL=$1

# requirements
# 1. routine-update, git, devscripts, git-buildpackage
# 2. team membership in Debian R Team
# 3. Debian Developer uploading privilages

# assumptions:
# 0. The root folder for packaging work is ~/debian/
# 1. All r-bioc-* packages are already checked out at ~/debian/
# 2. sbuild is used for package building

for pkg in $(cat bioc-${BIOC_VERSION}-level-${LEVEL}-packages.txt)
do
	echo "I: Processing package ${pkg}"
	echo -ne "\033]0;${pkg}\007"  # update the terminal title
	cd "${HOME}/debian/${pkg}"
	git fetch --all
	git remote set-head origin -a
	git checkout "$(git rev-parse --abbrev-ref origin/HEAD | sed 's=origin/==')"
	gbp pull
	git merge --ff debian/experimental
	if grep $DEBEMAIL debian/control ; then
		dch -D unstable 'Upload to unstable.'
	else
		dch --team -D unstable 'Upload to unstable.'
	fi
	debcommit -a
	git clean -fdx && gbp buildpackage --git-builder="sbuild --extra-repository='deb http://incoming.debian.org/debian-buildd/ buildd-unstable main'"
	git clean -fdx && gbp tag && dpkg-buildpackage -S -d
	debrelease  -S --dput && gbp push
done
