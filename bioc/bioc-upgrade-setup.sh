#!/bin/bash
set -e
set -u
set -o pipefail
set -x

# apt-get install -y ben jq dose-distcheck javascript-common ocaml-findlib python3-yaml

BIOC_VERSION=3.20
OLD_BIOC_VERSION=3.19
ben download
cat > bioc-${BIOC_VERSION}-ben.monitor <<-EOT
	title = "r-api-bioc-${BIOC_VERSION}";
	architectures = ["amd64"; "arm64"; "mips64el"; "ppc64el"; "riscv64"];
	is_affected = .depends ~ /r-api-bioc/ | .source ~ /r-bioc-/;
	is_good = .depends ~ "r-api-bioc-${BIOC_VERSION}";
	is_bad = .depends ~ "r-api-bioc-${OLD_BIOC_VERSION}";
EOT
ben monitor -stdin  -f json < bioc-${BIOC_VERSION}-ben.monitor > bioc-${BIOC_VERSION}-ben-output.json
for level in $(jq -r '. | keys | join("\n")' bioc-${BIOC_VERSION}-ben-output.json | sort -h) ; do
	jq -r ".[\"${level}\"] | keys | join(\"\n\")" < bioc-${BIOC_VERSION}-ben-output.json | grep -v r-bioc-biocgenerics > "bioc-${BIOC_VERSION}-level-${level}-packages.txt"
done
# r-bioc-biocgenerics provides "r-bioc-api-", and thus must be processed manually first 
